/// <reference path="./Mascara.ts"/>

module Ciebit.Mascaras
{
    export class Controlador
    {
        private campo:HTMLInputElement;
        private mascara:Mascara;
        private teclasIgnoradas:Array<String>;

        public constructor(Campo:HTMLInputElement, Mascara:Mascara)
        {
            this.campo = Campo;
            this.mascara = Mascara;
            this.teclasIgnoradas = [
                'AltLeft',
                'AltRight',
                'ArrowDown',
                'ArrowLeft',
                'ArrowRight',
                'ArrowUp',
                'Backspace',
                'CapsLook',
                'ControlLeft',
                'ControlRight',
                'Escape',
                'End',
                'F1', 'F2', 'F3', 'F4', 'F5', 'F6',
                'F7', 'F8', 'F9', 'F10', 'F11', 'F12',
                'Home',
                'ShiftLeft',
                'MetaLeft',
                'MetaRight',
                'ShiftRight',
                'Tab'
            ];

            this.instalar();
        }

        private aplicarMascara(evento:KeyboardEvent): this
        {
            let campo = this.obterCampo();
            let mascara = this.obterMascara();

            let totalCaracteres = campo.value.length;
            let posicaoCursor = 0;

            if (this.teclaIgnorada(evento.key)) {
                return this;
            }

            if (campo.selectionStart) {
                posicaoCursor = campo.selectionStart;
            }

            campo.value = mascara.mascarar(campo.value);

            //Calculando posição
            totalCaracteres -= posicaoCursor;
            posicaoCursor = campo.value.length - totalCaracteres;

            //Alterando posição do cursor
            campo.selectionStart = campo.selectionEnd = posicaoCursor;

            return this;
        }

        public definirMascara(Mascara:Mascara): this
        {
            this.mascara = Mascara;
            return this;
        }

        public definirTeclasIgnoradas(lista:Array<String>): this
        {
            this.teclasIgnoradas = lista;
            return this;
        }

        private instalar()
        {
            this.campo.addEventListener('keyup', (evento) => {
                this.aplicarMascara(evento);
            });
        }

        public obterCampo(): HTMLInputElement
        {
            return this.campo;
        }

        public obterMascara(): Mascara
        {
            return this.mascara;
        }

        private teclaIgnorada(teclaCodigo:string): boolean
        {
            return this.teclasIgnoradas.indexOf(teclaCodigo) >= 0;
        }
    }
}
