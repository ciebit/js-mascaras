/// <reference path="./Mascara.ts"/>

module Ciebit.Mascaras
{
    export class Telefone implements Ciebit.Mascaras.Mascara
    {
        public mascarar(telefone:string): string
        {
            if (! telefone) {
                return '';
            }

            let numero = telefone.replace(/\D/g, '');

            if (numero.length == 0) {
                return '';
            } else if (numero.length >= 11) {
                return '('+ numero.substr(0,2) +') ' + numero.substr(2,5) + '-'+ numero.substr(7,4);
            }

            return '('+ numero.substr(0,2) +') ' + numero.substr(2,4) + '-'+ numero.substr(6,4);
        }
    }
}
